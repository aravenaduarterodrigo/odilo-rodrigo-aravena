Se realiza aplicacion de Ansible, donde se construye imagen de docker hacia host cliente, los pasos para crear son:

Crear el archivo inventory.ini donde expongo los host clientes y local

Crear el achivo Dockerfile, donde agrego el puerto de escucha para la imagen de docker

Crear el archivo build.yml, donde se contruye la imagen de Docker a traves de los archivos previamente creados

Crear el archivo save.yml, donde me permite exportar la imagen de docker como un archivo tar.

Crear el archivo load.yml, donde me permite cargar el la imagen de docker haciae el host del cliente

Ejecucion

Una vez que esten creados los archivos la sintaxis y el orden de ejecucion es la siguiente:

ansible-playbook -i inventory.ini build.yml

ansible-playbook -i inventory.ini save.yml

ansible-playbook -i inventory.ini load.yml

Finalmente, para probar estando en el host cliente se debe realizar los siguientes comandos:

1.- ejecutar contenedor: root@192.168.100.38 ~]# docker run --rm -it -p 8080:8080 democontainer:v1.0

probar: [root@192.168.100.38 ~]# telnet localhost 8080
Trying ::1...
Connected to localhost.
Escape character is '^]'.
hello!

