Se crea el archivo node.js donde se crea una aplicacion de "Hola Mundo" utilizando la depencdia express.

Se crea el archivo server.js donde me permite definir la configuracion web utilizando la dependendencia express.js e indicar el puerto de salida.

Se crea el archivo Dockerfile donde me permite confiruar la nueva imagen de Docker

Se crea el archivo .dockerignore donde ignoro los modulos node y logs.

Se construte la imagen a partir de este comando:

docker build . -t raravena/node-web-app

Obteniendo este resultado:

docker images


REPOSITORY                      TAG        ID              CREATED

raravena/node-web-app    latest     d64d3505b0d2    1 minute ago

Finalmente ejecuto la imagen creada con el siguinete comando:

docker run -p 49160:8080 -d raravena/node-web-app

Donde se valida en el navegador digitando localhost:49160 y obtengo el texto "Hello World"

